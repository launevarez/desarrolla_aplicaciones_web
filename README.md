**desarrollo aplicaciones web con conexion a base de datos**
**Nevárez Calderón Laura Yoliztli**

- Práctica #1 - 02/09/2022 - Práctica de ejemplo
commit: 5e79054443833aa1542709421ac8485f2bc53324
Archivo: https://gitlab.com/launevarez/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html


- Práctica #2 - 09/09/2022 - Práctica de Java Script
commit: 0a1cfbd91d193621ce2d85f4b1673daed6c49079
Archivo: https://gitlab.com/launevarez/desarrolla_aplicaciones_web/-/blob/main/parcial1/practicaJavaScript.html

- Practica #3 - 15/09/2022 - Practica web con bases de datos- Parte 1
commit: 16d6765d0ea04e933ff19089d11c88fa20434bd5

- Práctica #4 - 19/09/2022 - Practica web con datos - Vista de consulta de datos
Commit: cd76e12d

- Práctica #5 - 22/09/2022 - Vista de registro de datos
commit: 635cedf1a085e3b1fa4eb7385e29b21c0deed154

- Práctica #6 -  26/09/2022 - Mostrar registro de consulta de datos
commit: 9a36f165e81a9be3c3d0ee9367bd4eed05abb5a9
